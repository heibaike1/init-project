



# 介绍
## 目的
这是一个个人的初始项目，主要目的是搭建一个通用的初始化项目框架。方便后期快速的开始一个项目。


## 系统需求
- JDK >= 1.8
- MySQL >= 5.7
- Maven >= 4.0
- redis


## 项目结构

核心模块注解
```angular2html

init-project 
├── init-common           // 工具类
│       └── annotation                    // 自定义注解
│       └── config                        // 全局配置
│       └── constant                      // 通用常量
│       └── core                          // 核心控制
│       └── enums                         // 通用枚举
│       └── exception                     // 通用异常
│       └── json                          // JSON数据处理
│       └── utils                         // 通用类处理
│       └── xss                           // XSS过滤处理
├── init-framework         // 框架核心
│       └── aspectj                       // 注解实现
│       └── config                        // 系统配置
│       └── datasource                    // 数据权限
│       └── interceptor                   // 拦截器
│       └── manager                       // 异步处理
│       └── shiro                         // 权限控制
│       └── web                           // 前端控制
├── init-generator   // 代码生成（不用可移除）
├── init-quartz      // 定时任务（不用可移除）
├── init-system      // 系统代码
├── init-admin       // 后台服务 
├── init-xxxxxx      // 其他模块

```

所有基础文件夹注解
```angular2html
init-project
├─init-admin        // 后台服务
│  └─src
│      └─main
│          ├─java
│          │  └─com
│          │      └─haixin
│          │          └─initadmin
│          │              └─controller      // 控制层
│          │                  └─system          // 核心系统模块接口
│          └─resources      // 资源文件
│              └─mybatis        // mybatis配置
├─init-common       // 工具类
│  └─src
│      └─main
│          └─java
│              └─com
│                  └─haixin
│                      └─initcommon
│                          ├─annotation     // 自定义注解
│                          ├─config         // 全局配置
│                          ├─constant       // 通用常量
│                          ├─core           // 核心控制
│                          │  ├─domain          // 实体封装
│                          │  │  ├─entity           // 实体
│                          │  │  └─model            // 登录实体
│                          │  ├─redis           // redis相关配置
│                          │  └─text            // 文本工具类
│                          ├─enums          // 通用枚举
│                          ├─exception      // 通用异常
│                          │  ├─base            // 异常基类
│                          │  └─user            // 用户相关异常
│                          ├─filter         // 通用流操作
│                          ├─utils          // 通用类处理
│                          │  ├─bean            // bean工具类
│                          │  ├─http            // http工具类
│                          │  ├─ip              // ip工具类
│                          │  ├─poi             // excel工具类
│                          │  ├─spring          // spring工具类
│                          │  └─uuid            // uuid工具类
│                          └─xss            // XSS过滤处理
├─init-framework         // 框架核心
│  └─src
│      └─main
│          └─java
│              └─com
│                  └─haixin
│                      └─initframework
│                          ├─config         // 系统配置
│                          │  ├─datasource      // 数据源配置
│                          │  └─properties      // 配置文件
│                          ├─interceptor    // 拦截器
│                          │  └─impl            // 拦截器实现类
│                          ├─manager        // 异步处理
│                          │  └─factory        // 工厂类
│                          ├─security       // 权限控制
│                          │  ├─context         // 上下文校验
│                          │  ├─filter          // 过滤器
│                          │  └─handle          // 拦截器
│                          └─web            // 前端控制
│                              └─service    // 业务层
├─init-system           // 系统代码
│  └─src
│      └─main
│          ├─java
│          │  └─com
│          │      └─haixin
│          │          └─initsystem
│          │              ├─domain         // 实体封装
│          │              │  └─vo          // 输出封装
│          │              ├─mapper         // 数据库操作
│          │              └─service        // 业务层 
│          │                  └─impl       // 业务层实现
│          └─resources      // 资源文件
│              └─mapper     // mapper文件
│                  └─system        // 系统模块sql
└─sql       // sql文件

```


package com.haixin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

//@ComponentScan(basePackages = {"com.haixin.initadmin.**", "com.haixin.initframework.**", "com.haixin.initcommon.**"})
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class InitAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(InitAdminApplication.class, args);
    }

}

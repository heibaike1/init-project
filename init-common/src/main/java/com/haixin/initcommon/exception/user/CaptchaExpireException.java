package com.haixin.initcommon.exception.user;

import com.haixin.initcommon.exception.UserException;

/**
 * 验证码失效异常类
 * 
 * @author
 */
public class CaptchaExpireException extends UserException
{
    private static final long serialVersionUID = 1L;

    public CaptchaExpireException()
    {
        super("user.jcaptcha.expire", null);
    }
}

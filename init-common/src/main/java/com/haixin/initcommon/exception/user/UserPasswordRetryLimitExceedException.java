package com.haixin.initcommon.exception.user;

import com.haixin.initcommon.exception.UserException;

/**
 * 用户错误最大次数异常类
 * 
 * @author
 */
public class UserPasswordRetryLimitExceedException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserPasswordRetryLimitExceedException(int retryLimitCount, int lockTime)
    {
        super("user.password.retry.limit.exceed", new Object[] { retryLimitCount, lockTime });
    }
}

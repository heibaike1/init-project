package com.haixin.initcommon.exception.user;

import com.haixin.initcommon.exception.UserException;

/**
 * 验证码错误异常类
 * 
 * @author
 */
public class CaptchaException extends UserException
{
    private static final long serialVersionUID = 1L;

    public CaptchaException()
    {
        super("user.jcaptcha.error", null);
    }
}

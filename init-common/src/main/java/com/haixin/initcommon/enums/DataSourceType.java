package com.haixin.initcommon.enums;

/**
 * 数据源
 * 
 * @author
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
